package Snake;

import Snake.factories.GameObjectFactory;

import java.util.*;
import java.util.function.Predicate;

public class Game
{
    private Snake snake;
    private GameField fieldSize;
    private List<GameObject> objects =  new ArrayList<GameObject>();;

    public void update()
    {
        if (!snake.getIsAlive())
            return;

        Point target = getTarget();
        if (snake.getBody().contains(target) && !snake.getBody().getLast().equals(target))
        {
            snake.setIsAlive(false);
            return;
        }
        Optional<GameObject> toTouch = objects.stream()
                .filter(gameObject -> gameObject.getLocation().equals(target))
                .findFirst();
        if (toTouch.isPresent())
            toTouch.get().makeChanges(this, target);
        else
            snake.move(target);
        if (!snake.getIsAlive())
            return;
        if (snake.getLength() + objects.size() < fieldSize.getWidth() * fieldSize.getHeight())
            controlAppleQuantity();
    }

    private Point getTarget()
    {
        int dx = snake.getDirection().getOffset().getX();
        int dy = snake.getDirection().getOffset().getY();
        int snakeX = snake.getHead().getX();
        int snakeY = snake.getHead().getY();
        return fieldSize.controlBorders(snakeX + dx, snakeY + dy, snake.getHead());
    }

    public Game(Snake snake, GameField fieldSize)
    {
        this.snake = snake;
        this.fieldSize = fieldSize;
    }

    public Game(GameField fieldSize)
    {
        this(null, fieldSize);
        initLevel();
    }

    public Snake getSnake()
    {
        return snake;
    }

    public void addObject(GameObject object)
    {
        objects.add(object);
    }

    public void addPortals(Point portal1, Point portal2){
        Portal p1 = new Portal(portal1);
        Portal p2 = new Portal(portal2);
        p1.exit = p2;
        p2.exit = p1;
        objects.add(p1);
        objects.add(p2);
    }

    public List<GameObject> getObjects()
    {
        return objects;
    }

    public GameField getFieldSize()
    {
        return fieldSize;
    }

    public void addObject(GameObjectType objectType)
    {
        Predicate<Point> checkCollision = (randomPoint) -> getSnake().getBody().contains(randomPoint) ||
                getObjects().stream().anyMatch((o) -> o.getLocation().equals(randomPoint));

        GameObjectFactory factory = objectType.getFactory();
        for ( GameObject gameObject : factory.addObject(objectType, () -> fieldSize.findFreePoint(checkCollision)))
            this.addObject(gameObject);
    }

    private void initLevel()
    {
        Deque<Point> snake_body = new ArrayDeque<Point>();
        snake_body.addFirst(new Point(1, 1));
        snake = new Snake(snake_body, 0, Direction.None);
        for (int i = 0; i < fieldSize.getWidth(); i++)
            for (int j = 0; j < fieldSize.getHeight(); j++)
            {
                if ((i == 0 || j == 0 || i == fieldSize.getWidth() - 1 || j == fieldSize.getHeight() - 1) && i != 5)
                    addObject(new Indestructable(new Point(i, j), GameObjectType.Wall));
            }
        addObject(GameObjectType.Apple);
        addObject(GameObjectType.Strawberry);
        addObject(GameObjectType.Pill);
        for (int i = 0; i < 15; i++)
            addObject(GameObjectType.Stone);
        addObject(GameObjectType.Portal);
        addObject(GameObjectType.Mushroom);
    }

    public void controlAppleQuantity()
    {
        long apples = getObjects().stream()
                .filter(x -> x.getGameObjectType() == GameObjectType.Apple)
                .count();
        if (apples == 0)
            addObject(GameObjectType.Apple);
    }

    public void restart()
    {
        objects.clear();
        initLevel();
    }

    public void setSnakeDirection(Direction direction)
    {
        snake.setDirection(direction);
    }

}
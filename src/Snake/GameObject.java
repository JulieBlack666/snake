package Snake;

public class GameObject
{
    public GameObjectType getGameObjectType()
    {
        return type;
    }

    public Point getLocation()
    {
        return location;
    }

    private GameObjectType type;
    private Point location;

    public GameObject(Point location, GameObjectType gameObjectType)
    {
        this.location = location;
        this.type = gameObjectType;
    }

    public GameObjectType getType() {
        return type;
    }

    @Override
    public String toString()
    {
        return "GameObject{" +
                "type=" + type +
                ", location=" + location +
                '}';
    }

    public void makeChanges(Game game, Point target)
    {
        Snake snake = game.getSnake();
        snake.addScore(type.getScores());
         if (type.isIncrease())
             snake.increase(target);
         if (type.isDecrease())
             snake.decrease();
         if (type.isPsycho())
             snake.bePsycho();
    }
}
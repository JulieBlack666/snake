package Snake;

public class Indestructable extends GameObject {
    public Indestructable(Point location, GameObjectType gameObjectType) {
        super(location, gameObjectType);
    }

    @Override
    public void makeChanges(Game game, Point target) {
        game.getSnake().setIsAlive(false);
    }

}

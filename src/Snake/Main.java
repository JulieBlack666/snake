package Snake;

import javax.swing.*;
import java.awt.*;

public class Main
{
    private static Game initGame()
    {
        return new Game(new GameField(40, 20));
    }

    public static void main(String[] args)
    {
        Game game = initGame();
        JFrame frame = new JFrame("SNAKE");
        frame.setContentPane(new GameBoard(game));
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(GameBoard.realWidth, GameBoard.realHeight));
        frame.setVisible(true);
    }
}


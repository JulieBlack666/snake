package Snake;

public class Destructable extends GameObject{
    public Destructable(Point location, GameObjectType gameObjectType) {
        super(location, gameObjectType);
    }

    @Override
    public void makeChanges(Game game, Point target) {
        game.getSnake().move(this.getLocation());
        game.getObjects().remove(this);
        super.makeChanges(game, target);
    }
}

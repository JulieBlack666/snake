package Snake;

public class Portal extends GameObject {
    public Portal exit;

    public Portal(Point location) {
        super(location, GameObjectType.Portal);
    }

    public void makeChanges(Game game, Point target) {
        game.getSnake().move(exit.getLocation());
    }
}

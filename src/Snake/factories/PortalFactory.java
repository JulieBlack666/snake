package Snake.factories;

import Snake.GameObjectType;
import Snake.Point;
import Snake.Portal;

import java.util.function.Supplier;

public class PortalFactory implements GameObjectFactory<Portal> {
    public static PortalFactory instance = new PortalFactory();
    @Override
    public Portal[] addObject(GameObjectType gameObjectType, Supplier<Point> pointSupplier) {
        Point point1 = pointSupplier.get();
        Point point2 = pointSupplier.get();
        while(point1.equals(point2))
            point2 = pointSupplier.get();
        Portal portal1 = new Portal(point1);
        Portal portal2 = new Portal(point2);
        portal1.exit = portal2;
        portal2.exit = portal1;
        return new Portal[] { portal1, portal2 };
    }
}

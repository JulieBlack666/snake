package Snake.factories;

import Snake.*;

import java.util.function.Supplier;

public class IndestructableFactory implements GameObjectFactory<Indestructable> {
    public static IndestructableFactory instance = new IndestructableFactory();
    @Override
    public Indestructable[] addObject(GameObjectType gameObjectType, Supplier<Point> pointSupplier) {
        return new Indestructable[] { new Indestructable(pointSupplier.get(), gameObjectType) };
    }
}

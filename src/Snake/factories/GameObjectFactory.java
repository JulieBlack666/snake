package Snake.factories;

import Snake.GameObject;
import Snake.GameObjectType;
import Snake.Point;

import java.util.function.Supplier;

public interface GameObjectFactory<T extends GameObject> {
    T[] addObject(GameObjectType gameObjectType, Supplier<Point> pointSupplier);
}

package Snake.factories;

import Snake.GameObject;
import Snake.GameObjectType;
import Snake.Point;

import java.util.function.Supplier;

public abstract class AbstractFactory<T extends GameObject> {
    public abstract T[] addObject(GameObjectType gameObjectType, Supplier<Point> pointSupplier);
}

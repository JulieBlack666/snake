package Snake.factories;

import Snake.Destructable;
import Snake.GameObjectType;
import Snake.Point;

import java.util.function.Supplier;

public class DestructableFactory implements GameObjectFactory<Destructable> {
    public static DestructableFactory instance = new DestructableFactory();

    @Override
    public Destructable[] addObject(GameObjectType gameObjectType, Supplier<Point> pointSupplier) {
        return new Destructable[] { new Destructable(pointSupplier.get(), gameObjectType) } ;
    }
}

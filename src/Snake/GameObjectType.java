package Snake;

import Snake.factories.GameObjectFactory;
import Snake.factories.DestructableFactory;
import Snake.factories.IndestructableFactory;
import Snake.factories.PortalFactory;

public enum GameObjectType
{
    Strawberry(100, true, false, false, DestructableFactory.instance),
    Apple(10, true, false, false, DestructableFactory.instance),
    Pill(0, false, true, false, DestructableFactory.instance),
    Mushroom(0, false, false, true, DestructableFactory.instance),
    Wall(0, false, false, false, IndestructableFactory.instance),
    Stone(0, false, false, false, IndestructableFactory.instance),
    Portal(0, false, false, false, PortalFactory.instance);

    private final int scores;
    private final boolean increase;
    private final boolean decrease;
    private final boolean psycho;
    private final GameObjectFactory factory;

    GameObjectType(int scores, boolean increase, boolean decrease, boolean psycho, GameObjectFactory factory)
    {
        this.scores = scores;
        this.increase = increase;
        this.decrease = decrease;
        this.psycho = psycho;
        this.factory = factory;
    }

    public int getScores()
    {
        return scores;
    }

    public boolean isIncrease()
    {
        return increase;
    }

    public boolean isDecrease()
    {
        return decrease;
    }

    public boolean isPsycho() { return psycho; }

    public GameObjectFactory getFactory() {
        return factory;
    }

}

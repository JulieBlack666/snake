package Snake;

public enum Direction
{
    Up(new Point(0, -1)),
    Down(new Point(0, 1)),
    Left(new Point(-1, 0)),
    Right(new Point(1, 0)),
    None(new Point(0, 0));

    private Point offset;

    Direction(Point offset)
    {
        this.offset = offset;
    }

    public Point getOffset()
    {
        return this.offset;
    }

    public boolean isOpposite(Direction other)
    {
        return this.offset.getX() + other.offset.getX() == 0 && this.offset.getY() + other.offset.getY() == 0;
    }
}
